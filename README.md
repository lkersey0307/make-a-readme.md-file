# Writing a Markdown File with Bitbucket #
***********

This is a general introduction to creating and editing markdown files with the web-based hosting service known as Bitbucket.  

# Why Markdown? #
***********

Markdown is useful for documenting and displaying the steps of a process--particularly when those steps involve digital tools. Markdown has the following advantages:

- Offers users a constrained set of choices compared to other text formatting systems, which often results in a professional-looking product that will meet the expectations of readers who are familiar with markdown files (e.g., computer programmers and engineers) 
- Encourages writers to think carefully and critically about their formatting choices (especially at the beginning when you'll likely have to go out of your way to look up the syntax for certain formats)
- Easy to display alongisde other digital materials (e.g., code and data) 

# Get Started #
***********

### Download Git
If you've never used Git before, you may need to [download the latest version](https://git-scm.com/downloads) for your operating system. Or if you're using Linux, you can install Git with a sudo command:	
```r
sudo apt install git
```
###Create a [Bitbucket](https://bitbucket.org/account/signin/) account, or log in to an existing account 
###Create a new repository
	* Click the `+` sign in the lefthand menu. 
	* A new menu titled "CREATE A NEW" will appear. Select `Repository`
	* Give your repository a descriptive name. I called this repo "Make a README.md file"
	* Choose your access level and set your repo type to "Git"
###Create the markdown "README" file
	* Click the blue `Create README` button
	* Get a feel for writing in the markdown language by entering some text. 
	* Click the blue `Commit` button at the bottom right of your screen to save your changes. Another window will appear. Click "Commit" again 
	* Click on the `<> Source` or the `Overview` tab in the lefthand menu to see your changes

# Clone your README to your local directory #
***********
To host your markdown on another site (like tumblr), you'll want to dowload a version of it on your local computer (i.e., not hosted on Bitbucket). You only need to do this once. 

- Click on the `Overview` tab in the lefthand menu
- Copy the HTTPS link at the top of the page 
- Open a commmand prompt or terminal 	

	#####For PC Users:
	* Press Win+R on your keyboard
	* Type cmd.exe
	* Press Enter 
	
	#####For Mac Users:
	* Click Command + Space to access your Spotlight search tool
	* Type Terminal 
	* Press Enter 
	
- A local version of your new Git repository will be imported to your current working directory. To make it easy to find this directory, set your working directory to your home directory

	#####PC users, type the following into your command prompt:
	```
	cd %HOMEPATH%
	```
	#####Mac and Linux users, type the following into your terminal:
	```
	cd ~
	```
	
- In the command prompt/terminal, type git clone followed by the HTTPS link 
- In your home directory, you will now find a folder with the same name as your Git repository. You will find your README file in this folder. 

# Edit until your markdown file is perfect #
***********

### Edit from your Bitbucket page ###
* Click `Overview` 
* Click `Edit README`
* Add, remove, or edit text
* Click `Commit` to save changes. Click `Commit` again 
* In your command prompt/terminal, set your working directory to be your new Git repository folder by typing `cd` followed by the name of your repository. For example, I would type the following:
```
cd make-a-readme.md-file
```
* In your command prompt/terminal, type 'git pull' to pull the most recent version of the README into your local copy. 

### Edit from your local copy ###
You can also edit the local version of your README file. To push the most up-to-date version of your README onto Bitbucket, open a command prompt/terminal and type the following:
```
cd *name of your Git repository*
git commit -a -m "updated the README file"
git push origin master
```
## General Suggestions  
***********
* **Don't miss a step**. If you're using markdown to document a process, be thorough and include every step. Your readers will assume that you have included each stage of the process. Omitting even a seemingly obvious, minute step will cause errors or frustration. 
* [Learn more about about Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Author ##
***********
Lauren Kersey 